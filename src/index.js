import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import {  ApolloProvider } from 'react-apollo';

import App from './components/App';
import ArticleList from './components/ArticleList';
import Article from './components/Article';

const URI = `http://localhost:4000/graphql`;

const client = new ApolloClient({
  networkInterface: createNetworkInterface({ uri: URI})
});

const Root = () => {
  return (
    <ApolloProvider client={client}>
      <Router history={hashHistory}>
        <Route path="/" component={App}>
          <IndexRoute component={ArticleList} />
          <Route path="/:id" component={Article} />
        </Route>
      </Router>
    </ApolloProvider>
  );
}
ReactDOM.render(
  <Root />,
  document.getElementById('root'),
);
