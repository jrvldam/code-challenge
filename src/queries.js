import gql from 'graphql-tag'

export const ARTICLES_QUERY = gql`
{
  articles {
    author
    excerpt
    id
    title
  }
}`;

export const ARTICLE_QUERY = gql`
query article($id: ID!) {
  article(id: $id) {
    id
    author
    content
    published
    tags
    title
  }
} 
`;
