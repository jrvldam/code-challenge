import React from 'react';

function Header() {
  return (
    <header className="header">
      <h2>board of cards</h2>
      <small>billin code challenge</small>
    </header>
  );
}

export default Header;
