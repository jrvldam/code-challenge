import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { Link } from 'react-router';

import Header from './Header';
import Footer from './Footer';

import { ARTICLES_QUERY } from '../queries';

class ArticleList extends Component {
  renderArticles() {
    return this.props.data.articles.map(({ id, author, excerpt }) => {
      return (
        <li key={ id } className="card">
          <Link to={`/${id}`}>
            <article>
              <h4>{author}</h4>
              <p>{excerpt}</p>
            </article>
          </Link>
        </li>
      );
    });
  }

  render() {
    if (this.props.data.loading) return <div className="board">Loading...</div>;

    return (
      <div>
        <Header />
        <div className="board">
          <ul className="list">
            { this.renderArticles() }
          </ul>
        </div>
        <Footer />
      </div>
    );
  }
}

export default graphql(ARTICLES_QUERY)(ArticleList);
