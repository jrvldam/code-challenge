import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { Link } from 'react-router';

import { ARTICLE_QUERY } from '../queries';
import Header from './Header';
import Footer from './Footer';

class Article extends Component {
  renderTags(tags) {
    return tags.map(tag => <li key={tag}>{tag}</li>);
  }

  render() {
    const { article } = this.props.data;

    if (!article) return <div>Loading...</div>;

    const { author, content, published, tags, title } = article;

    return (
      <div>
        <Header />
        <Link to="/" className="back-btn">Back</Link>
        <div className="article-view">
          <h3>{title}</h3>
          <h4>By: {author}</h4>
          <p className="right">Published: { published ? 'Yes' : 'Not'  }</p>
          <p>{content}</p>
          Tags:
          <ul>
            { this.renderTags(tags) }
          </ul>
        </div>
        <Footer />
      </div>
    );
  }
}

export default graphql(ARTICLE_QUERY, {
  options: props => ({ variables: { id: props.params.id } })
})(Article);
